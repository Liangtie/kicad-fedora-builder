#!/bin/sh

set -e
set -x

# GitLab Project ID.
GITLAB_ID_KICAD=15502567	# https://gitlab.com/kicad/code/kicad

# URLs for libs and docs.
GITLAB_URL_FOOTPRINTS="https://gitlab.com/kicad/libraries/kicad-footprints.git"
GITLAB_URL_PACKAGES3D="https://gitlab.com/kicad/libraries/kicad-packages3D.git"
GITLAB_URL_SYMBOLS="https://gitlab.com/kicad/libraries/kicad-symbols.git"
GITLAB_URL_TEMPLATES="https://gitlab.com/kicad/libraries/kicad-templates.git"
GITLAB_URL_DOC="https://gitlab.com/kicad/services/kicad-doc.git"

# Get the date of the HEAD commit in a repo.
get_last_date () {
	local PROJECT_ID="$1"
	local API_URL="${GITLAB_API_PREFIX}${PROJECT_ID}${GITLAB_API_SUFFIX}"
	sleep 1
	local LAST_DATE="$(curl -s ${API_URL} | jq -r '.created_at')"
	echo "$(date --utc --date=${LAST_DATE} +%Y%m%d)"
}

# Get the SHA of the HEAD commit in a repo.
get_last_rev () {
	local PROJECT_ID="$1"
	local API_URL="${GITLAB_API_PREFIX}${PROJECT_ID}${GITLAB_API_SUFFIX}"
	sleep 1
	echo "$(curl -s ${API_URL} | jq -r '.id')"
}

# Get the latest tag (on any branch).  The tag must begin with a digit to
# filter out tags like v5.
get_last_tag () {
	local URL="$1"
	sleep 1
	echo "$(git ls-remote --tags ${URL} | sed -E -e 's,.*/,,' -e 's,[-^].*,,' | grep -E "^[[:digit:]]" | tail -1)"
}

show_help () {
	set +x
	echo "$0 [-c COPR_ID] [-m MOCK_TARGET] [-f] [-h]" >&2
	echo "" >&2
	echo "  -h shows this help message." >&2
	echo "  -b BRANCH specifies the branch. This flag is mandatory." >&2
	echo "  -c COPR_ID performs a remote COPR build using the specified ID." >&2
	echo "  -m MOCK_TARGET performs a local MOCK build for the specified target." >&2
	echo "  -f forces all packages to be (re-)built." >&2
	echo "" >&2
	echo "If neither -c nor -m is specified, then no build will be done, but" >&2
	echo "the SPECs will still be prepared, and can be used to manually kick off" >&2
	echo "a build at a later time." >&2
	echo "" >&2
	echo "example: $0 -b 7.0 -c your_copr_id/kicad-testing" >&2
	echo "example: $0 -b 7.0 -m fedora-rawhide-x86_64" >&2
	set -x
}

# Start of main shell script.
COPR_ID=
MOCK_TARGET=
BUILD_KICAD=
BRANCH=
while getopts "hb:c:m:f" opt; do
	case "$opt" in
		h)
			show_help
			exit 1
			;;
		b)
			BRANCH="${OPTARG}"
			;;
		c)
			COPR_ID="${OPTARG}"
			;;
		m)
			MOCK_TARGET="${OPTARG}"
			;;
		f)
			BUILD_KICAD="TRUE"
			;;
		\?)
			set +x
			echo "Invalid option: -${OPTARG}" >&2
			set -x
			show_help
			exit 1
			;;
		:)
			set +x
			echo "Option -${OPTARG} requires an argument." >&2
			set -x
			show_help
			exit 1
			;;
	esac
done

if [ -z "${BRANCH}" ]
then
	set +x
	echo "The branch must be set by specifying the \"-b BRANCH\" flag." >&2
	echo "" >&2
	set -x

	show_help
	exit 1
fi

# GitLab API URL prefix and suffix (to be combined with Project ID).
GITLAB_API_PREFIX="https://gitlab.com/api/v4/projects/"
GITLAB_API_SUFFIX="/repository/commits/${BRANCH}"

# The version of the packages will be set to the current KICAD_SEMANTIC_VERSION
# defined in the KiCad source code.  This is the URL to the file where the
# corresponding CMake variable is defined.
KICAD_VERSION_URL="https://gitlab.com/kicad/code/kicad/-/raw/${BRANCH}/cmake/KiCadVersion.cmake"

# Prepare directories.
ARTIFACTS=build/artifacts
RPMBUILD=build/rpmbuild
mkdir -p ${ARTIFACTS}
mkdir -p ${RPMBUILD}/{SOURCES,SPECS,SRPMS}

# Get current version of the branch.
KICAD_VERSION_FILE="$(curl -s ${KICAD_VERSION_URL})"
KICAD_VERSION="$(grep -m 1 "KICAD_SEMANTIC_VERSION" <<< ${KICAD_VERSION_FILE} | grep -o -E "[[:digit:]\.]+" | head -n 1)"

# Query information about latest commits.
SNAPDATE_KICAD=$(get_last_date ${GITLAB_ID_KICAD})
COMMIT_KICAD=$(get_last_rev ${GITLAB_ID_KICAD})

# Get the tags for the other components.  They should all be the same, but who
# knows...
DOCTAG=$(get_last_tag ${GITLAB_URL_DOC})
SYMTAG=$(get_last_tag ${GITLAB_URL_SYMBOLS})
FOOTTAG=$(get_last_tag ${GITLAB_URL_FOOTPRINTS})
P3DTAG=$(get_last_tag ${GITLAB_URL_PACKAGES3D})
TPLTAG=$(get_last_tag ${GITLAB_URL_TEMPLATES})

# Check if current revisions were already built before.
grep -q "${COMMIT_KICAD}" ${RPMBUILD}/SPECS/* || BUILD_KICAD="TRUE"

# (Re-)Create the SPEC files.
set +x
echo "Generating SPECs." >&2
set -x

# The version of the packages follows the Fedora Packaging Guidelines and will
# be set to <apprel>-<pkgrel>.<snapinfo> (e.g., 5.99.0-1.20210227git061218e).
# When multiple different snapshots from the same date are built, the order of
# versions is determined by the commit hash. This can lead to a situation where
# a newer build is not detected as an update for an older build. More complex
# logic would have to be implemented to detect such a case and bump <pkgrel> if
# required and automatically reset it back to "1" when <apprel> is increased.

SPEC_KICAD="${RPMBUILD}/SPECS/kicad-testing.spec"
sed \
	-e "s/@SNAPSHOTDATE@/${SNAPDATE_KICAD}/" \
	-e "s/@COMMITHASH0@/${COMMIT_KICAD}/" \
	-e "s/@VERSION@/${KICAD_VERSION}/" \
	-e "s/@DOCTAG@/${DOCTAG}/" \
	-e "s/@SYMTAG@/${SYMTAG}/" \
	-e "s/@FOOTTAG@/${FOOTTAG}/" \
	-e "s/@P3DTAG@/${P3DTAG}/" \
	-e "s/@TPLTAG@/${TPLTAG}/" \
	templates/kicad-testing.spec > ${SPEC_KICAD}
set +x
echo "Prepared ${SPEC_KICAD}." >&2
set -x

# Do a local mock build.
if [ -n "${MOCK_TARGET}" ]; then
	set +x
	echo "Generating SRPMs."
	set -x

	# Remove old source archives and SRPMs.
	ls -1 ${RPMBUILD}/SOURCES/ | \
	while read SOURCE; do
		SRCNAME=${SOURCE%%.*}
		HASH=${SRCNAME##*-}
		if ! grep -q "${HASH}" ${RPMBUILD}/SPECS/*; then
			rm -f ${RPMBUILD}/SOURCES/${SOURCE}
		fi
	done
	rm -f ${RPMBUILD}/SRPMS/*

	rpmbuild --define="_topdir ${RPMBUILD}" --undefine=_disable_source_fetch -bs "${SPEC_KICAD}"
	SRPM_KICAD=$(find ${RPMBUILD}/SRPMS/ -name "kicad-${KICAD_VERSION}*.src.rpm")
	set +x
	echo "Prepared ${SRPM_KICAD}."
	set -x

	set +x
	echo "Starting the local mock build."
	set -x

	MOCK_SRPMS=

	if [ -n "${BUILD_KICAD}" ]; then
		MOCK_SRPMS="${MOCK_SRPMS} ${SRPM_KICAD}"
	else
		set +x
		echo "Package kicad-testing-${KICAD_VERSION}-1.${SNAPDATE_KICAD}git${COMMIT_KICAD:0:7} was already built."
		set -x
	fi
	
	if [ -n "${MOCK_SRPMS}" ]; then
		mock -r "${MOCK_TARGET}" --resultdir="${ARTIFACTS}" --rebuild ${MOCK_SRPMS}
	fi
fi

# Do a remote copr build.  We allow 20 hours.  We shouldn't need that much,
# but the default of 5 hours is too short for the new zstd compression.
if [ -n "${COPR_ID}" ]; then
	set +x
	STATUS_LOCATION=$(echo ${COPR_ID} | sed -e 's/@/g\//')
	echo "Starting the remote copr build.  Check the status of the build here:"
	echo "https://copr.fedoraproject.org/coprs/${STATUS_LOCATION}/builds/"
	set -x

	if [ -n "${BUILD_KICAD}" ]; then
		copr-cli build --timeout 72000 --nowait "${COPR_ID}" "${SPEC_KICAD}"
	else
		set +x
		echo "Package kicad-testing-${KICAD_VERSION}-1.${SNAPDATE_KICAD}git${COMMIT_KICAD:0:7} was already built."
		set -x
	fi
fi

exit 0
