%global snapdate @SNAPSHOTDATE@
%global commit0 @COMMITHASH0@
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

Name:           kicad-nightly-packages3d
Version:        @VERSION@
Release:        1.%{snapdate}git%{shortcommit0}%{?dist}
Summary:        3D models for KiCad
License:        CC-BY-SA
URL:            https://kicad.org/libraries/

Source0:        https://gitlab.com/kicad/libraries/kicad-packages3D/-/archive/%{commit0}/kicad-packages3D-%{commit0}.tar.gz

BuildArch:      noarch

BuildRequires:  cmake
BuildRequires:  make

Recommends:     kicad-nightly

%description
KiCad is an open-source electronic design automation software suite for the
creation of electronic schematic diagrams and printed circuit board artwork.
This package provides the 3D models which are part of the official KiCad
libraries.


%prep

%autosetup -n kicad-packages3D-%{commit0}


%build

%cmake \
    -DKICAD_DATA=%{_datadir}/kicad-nightly
%cmake_build


%install

%cmake_install


%files
%{_datadir}/kicad-nightly/3dmodels/*.3dshapes/
%license LICENSE.md


%changelog
