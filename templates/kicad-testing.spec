%global snapdate @SNAPSHOTDATE@
%global commit0 @COMMITHASH0@
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})
%global doctag @DOCTAG@
%global symtag @SYMTAG@
%global foottag @FOOTTAG@
%global p3dtag @P3DTAG@
%global tpltag @TPLTAG@

Name:           kicad
Version:        @VERSION@
Release:        1000.%{snapdate}git%{shortcommit0}%{?dist}
Epoch:          1
Summary:        EDA software suite for creation of schematic diagrams and PCBs

License:        GPL-3.0-or-later
URL:            https://www.kicad.org

Source0:        https://gitlab.com/kicad/code/kicad/-/archive/%{commit0}/kicad-%{commit0}.tar.gz
Source1:        https://gitlab.com/kicad/services/kicad-doc/-/archive/%{doctag}/kicad-doc-%{doctag}.tar.gz
Source2:        https://gitlab.com/kicad/libraries/kicad-templates/-/archive/%{tpltag}/kicad-templates-%{tpltag}.tar.gz
Source3:        https://gitlab.com/kicad/libraries/kicad-symbols/-/archive/%{symtag}/kicad-symbols-%{symtag}.tar.gz
Source4:        https://gitlab.com/kicad/libraries/kicad-footprints/-/archive/%{foottag}/kicad-footprints-%{foottag}.tar.gz
Source5:        https://gitlab.com/kicad/libraries/kicad-packages3D/-/archive/%{p3dtag}/kicad-packages3D-%{p3dtag}.tar.gz


# https://gitlab.com/kicad/code/kicad/-/issues/237
ExclusiveArch:  x86_64 aarch64 ppc64le

BuildRequires:  boost-devel
BuildRequires:  chrpath
BuildRequires:  cmake
BuildRequires:  desktop-file-utils
BuildRequires:  gcc-c++
BuildRequires:  gettext
BuildRequires:  glew-devel
BuildRequires:  glm-devel
BuildRequires:  gtk3-devel
BuildRequires:  libappstream-glib
BuildRequires:  libcurl-devel
BuildRequires:  libngspice-devel
BuildRequires:  make
BuildRequires:  opencascade-devel
BuildRequires:  python3-devel
BuildRequires:  python3-wxpython4
BuildRequires:  shared-mime-info
BuildRequires:  swig
BuildRequires:  unixODBC-devel
BuildRequires:  wxGTK-devel
BuildRequires:  zlib-devel

# Documentation
BuildRequires:  po4a
BuildRequires:  rubygem-asciidoctor

Provides:       bundled(fmt) = 9.0.0
Provides:       bundled(libdxflib) = 3.26.4
Provides:       bundled(polyclipping) = 6.4.2
Provides:       bundled(potrace) = 1.15

Requires:       electronics-menu
Requires:       python3-wxpython4
Requires:       unixODBC

# libngspice is dlopen'ed so it is not strictly required, but recommended.
# We also recommend the models.
Recommends:     libngspice
Recommends:     ngspice-codemodel

# wxGTK has been patched to no longer need redhat-lsb-core, but this
# only happened in F38 and above.  So we keep the "recommends" for
# F37 and below.  Once F37 is obsolete, we can remove this completely.
%if 0%{?fedora} <= 37
Recommends:     redhat-lsb-core
%endif


Suggests:       kicad

%description
KiCad is EDA software to design electronic schematic
diagrams and printed circuit board artwork of up to
32 layers.

%package        packages3d
Summary:        3D Models for KiCad
License:        CC-BY-SA
BuildArch:      noarch
Requires:       kicad >= 7.0.0

%description    packages3d
3D Models for KiCad.

%package        doc
Summary:        Documentation for KiCad
License:        GPLv3+ or CC-BY
BuildArch:      noarch

%description    doc
Documentation for KiCad.


%prep
%setup -q -n kicad-%{commit0} -a 1 -a 2 -a 3 -a 4 -a 5


%build

# KiCad application
%cmake \
    -DKICAD_SCRIPTING_WXPYTHON=ON \
    -DKICAD_USE_OCC=ON \
    -DKICAD_INSTALL_DEMOS=ON \
    -DKICAD_BUILD_QA_TESTS=OFF \
    -DKICAD_SPICE=ON \
    -DKICAD_BUILD_I18N=ON \
    -DKICAD_I18N_UNIX_STRICT_PATH=ON \
    -DKICAD_USE_EGL=OFF \
    -DKICAD_VERSION_EXTRA=%{release} \
    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    -DPYTHON_SITE_PACKAGE_PATH=%{python3_sitearch}
%cmake_build

# Templates
pushd %{name}-templates-%{tpltag}/
%cmake
%cmake_build
popd

# Symbol libraries
pushd %{name}-symbols-%{symtag}/
%cmake
%cmake_build
popd

# Footprint libraries
pushd %{name}-footprints-%{foottag}/
%cmake
%cmake_build
popd

# 3D models
pushd %{name}-packages3D-%{p3dtag}/
%cmake
%cmake_build
popd

# Documentation (HTML only)
pushd %{name}-doc-%{doctag}/
%cmake \
    -DPDF_GENERATOR=none \
    -DBUILD_FORMATS=html
%cmake_build
popd


%install

# KiCad application
%cmake_install

# Binaries must be executable to be detected by find-debuginfo.sh
chmod +x %{buildroot}%{python3_sitearch}/_pcbnew.so

# Binaries are not allowed to contain rpaths
chrpath --delete %{buildroot}%{python3_sitearch}/_pcbnew.so

# Install desktop
for desktopfile in %{buildroot}%{_datadir}/applications/*.desktop ; do
  desktop-file-install \
  --dir %{buildroot}%{_datadir}/applications \
  --delete-original                          \
  ${desktopfile}
done

# Templates
pushd %{name}-templates-%{tpltag}/
%cmake_install
cp -p LICENSE.md ../LICENSE-templates.md
popd

# Symbol libraries
pushd %{name}-symbols-%{symtag}/
%cmake_install
cp -p LICENSE.md ../LICENSE-symbols.md
popd

# Footprint libraries
pushd %{name}-footprints-%{foottag}/
%cmake_install
cp -p LICENSE.md ../LICENSE-footprints.md
popd

# 3D models
pushd %{name}-packages3D-%{p3dtag}/
%cmake_install
popd

# Documentation
pushd %{name}-doc-%{doctag}/
%cmake_install
popd

%find_lang %{name}


%check
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.metainfo.xml


%files -f %{name}.lang
%doc AUTHORS.txt
%attr(0755, root, root) %{_bindir}/*
%{_libdir}/kicad/
%{_libdir}/libkicad_3dsg.so
%{_libdir}/libkicad_3dsg.so.2.0.0
%{python3_sitearch}/_pcbnew.so
%pycached %{python3_sitearch}/pcbnew.py
%{_datadir}/%{name}/
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/*.*
%{_datadir}/icons/hicolor/*/mimetypes/application-x-*.*
%{_datadir}/mime/packages/*.xml
%{_metainfodir}/*.metainfo.xml
%license LICENSE*
%exclude %{_datadir}/%{name}/3dmodels/*

%files packages3d
%{_datadir}/%{name}/3dmodels/*.3dshapes
%license %{name}-packages3D-%{p3dtag}/LICENSE*

%files doc
%{_docdir}/%{name}/help/
%exclude %{_docdir}/%{name}/AUTHORS.txt
%license %{name}-doc-%{doctag}/LICENSE*


%changelog
