#!/bin/sh

set -e
set -x

dnf install copr-cli curl jq git -y
dnf remove python3-progress --noautoremove -y

./builder-nightlies.sh -c "@kicad/kicad"

./builder-testing.sh -b 7.0 -c "@kicad/kicad-testing"

exit 0
